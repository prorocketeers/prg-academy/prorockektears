import { Context } from 'koa'
import SessionMap from './SessionMap'

const sessions = new SessionMap()

export function auth(ctx: Context, next:any){
  if (!ctx.cookies) {
    ctx.status = 401
    console.log('Missing cookie header.')
    return
  }

  const sessionId = ctx.cookies.get('sessionId') || ''
  if (!sessionId) {
    ctx.status = 401
    console.log('Missing sessionId cookie.')
    return
  }

  const user = sessions.getUser(sessionId)
  if (!user)  {
    ctx.status = 403
    console.log('Unknown user.')
    return
  }
  console.log('Logged user: ', user.nickname)
  return next()
}

