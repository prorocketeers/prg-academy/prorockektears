import { MovieOutput } from './movie.model';

interface WatchlistIput {
  movies: Array<string>;
}

interface WatchlistOutput {
  id: string;
  userRef: object;
  movies: Array<MovieOutput>;
}

export { WatchlistOutput, WatchlistIput };
