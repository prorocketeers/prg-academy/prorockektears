import { UserInput } from './user.model';
interface MovieInput {
  title: string;
  imdbId: string;
}

interface MovieOutput {
  id: string;
  title: string;
  imdbId: string;
  ratings: Array<MovieRating>; // TODO: Implement ratings
}

interface MovieRating {
  rating: number;
  user: {};
}
export { MovieInput, MovieOutput, MovieRating };
