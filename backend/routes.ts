import { UserInput, UserOutput, UserLogin } from './user.model'
import { MovieInput, MovieOutput, MovieRating } from './movie.model'
import { WatchlistOutput } from './watchlist.model'
import { getConnection } from './mongoDB'
import bcrypt from 'bcrypt'
import KoaRouter from 'koa-router'
import { Context } from 'koa'
import { ObjectID } from 'bson'
import { auth } from './auth'
import SessionMap from './SessionMap'

const routes = new KoaRouter()

const session = new SessionMap()

// route registration
routes.post('/users', async (ctx: Context) => {
  let user: UserInput = ctx.request.body
  let password = user.password

  let nickname = user.nickname
  let email = user.email

  // Check if nickname and email is unique
  let isInDb = async (): Promise<object | null> => {
    return await db
      .collection('users')
      .findOne({ $or: [{ email: email }, { nickname: nickname }] })
  }
  const saltRounds = 10
  const hash = await bcrypt.hash(password, saltRounds)
  // Store hash in your password DB.
  let userHash: UserInput = Object.assign({}, user, { password: hash })
  const db = await getConnection()
  try {
    let userExists = await isInDb()
    if (userExists) {
      ctx.status = 404
      return
    }
    const stuff = await db.collection('users').insertOne(userHash)
    const id: string = stuff.insertedId.toHexString()
    const res: UserOutput = {
      id: id,
      email: user.email,
      gender: user.gender,
      nickname: user.nickname,
    }
    ctx.response.body = res
  } catch (err) {
    console.log(err.stack)
  }
})

// route login
routes.post('/users/login', async (ctx: Context) => {
  const userLogin: UserLogin = ctx.request.body
  let password = userLogin.password
  const saltRounds = 10
  const hash = await bcrypt.hash(password, saltRounds)
  let userHash: UserLogin = Object.assign({}, userLogin, { password: hash })
  const db = await getConnection()
  try {
    const users = await db
      .collection('users')
      .find({ nickname: userHash.nickname })
      .toArray()
    for (let user of users) {
      const res = await bcrypt.compare(password, user.password)
      if (res) {
        ctx.status = 200
        const response: UserOutput = {
          id: user._id,
          email: user.email,
          gender: user.gender,
          nickname: user.nickname,
        }
        const sessionId = session.setUser(response)
        ctx.set('Set-Cookie', 'sessionId=' + sessionId + '; Path=/')
        ctx.response.body = response
      } else {
        ctx.status = 404
      }
    }
  } catch (err) {}
})

//route movies
routes.post('/movies', auth, async (ctx: Context) => {
  const userMovie: MovieInput = ctx.request.body
  const movie = {
    title: userMovie.title,
    imdbId: userMovie.imdbId,
    ratings: [],
  }
  const db = await getConnection()
  try {
    const stuff = await db.collection('movies').insertOne(movie)
    const id: string = stuff.insertedId.toHexString()
    const res: MovieOutput = {
      id: id,
      title: movie.title,
      imdbId: movie.imdbId,
      ratings: movie.ratings,
    }
    ctx.response.body = res
    console.log('Add movie result: ', res)
  } catch (err) {}
})

export const getUniqueId = (): string => {
  return Math.random().toString()
}

routes.get('/movies/:id', auth, async (ctx: Context) => {
  const id = ctx.params.id
  const db = await getConnection()
  try {
    const res = await db.collection('movies').findOne({ _id: new ObjectID(id) })
    ctx.response.body = res
    ctx.status = 200
  } catch (err) {
    ctx.status = 404
  }
})

routes.get('/movies', auth, async (ctx: Context) => {
  const db = await getConnection()
  try {
    const res = await db
      .collection('movies')
      .find()
      .toArray()
    ctx.response.body = res
    ctx.status = 200
  } catch (err) {
    ctx.status = 404
  }
})

// Watchlist - create watchlist in database
routes.post('/watchlist', auth, async (ctx: Context) => {
  const watchListId: string = getUniqueId()
  const movieIds = ctx.request.body
  const cookie = ctx.cookies.get('sessionId')
  if (!cookie) {
    ctx.status = 403
    return
  }

  let findWatchlist = async (
    watchlist: any
  ): Promise<WatchlistOutput | null> => {
    return await db.collection('watchlists').findOne({ user: watchlist.user })
  }

  const movies: Array<object> = movieIds.map((movieId: string) => {
    return { $ref: 'movies', $id: new ObjectID(movieId) }
  })

  const watchlist = {
    id: watchListId,
    user: { $ref: 'users', $id: session.getUser(cookie).id },
    movies: movies,
  }

  const db = await getConnection()
  try {
    let watchlistFromDb = await findWatchlist(watchlist)
    if (watchlistFromDb) {
      const filteredMovies = movies.filter((movieRefNew: any) => {
        return (
          watchlistFromDb &&
          !watchlistFromDb.movies.find((movieRefOld: any) => {
            return movieRefNew.$id.equals(movieRefOld.oid)
          })
        )
      })

      const result = await db
        .collection('watchlists')
        .update(
          { user: { $ref: 'users', $id: session.getUser(cookie).id } },
          { $push: { movies: { $each: filteredMovies } } }
        )
      ctx.response.body = result
    } else {
      const stuff = await db.collection('watchlists').insertOne(watchlist)
      ctx.response.body = stuff
    }
  } catch (err) {
    console.log('error', err)
  }
})

routes.post('/movies/:id/ratings', auth, async (ctx: Context) => {
  let movieId = ctx.params.id
  const cookie = ctx.cookies.get('sessionId')
  if (!cookie || !session.getUser(cookie)) {
    ctx.status = 403
    return
  }
  const movieRating: MovieRating = {
    rating: ctx.request.body.rating,
    user: { $ref: 'users', $id: session.getUser(cookie).id },
  }
  const db = await getConnection()
  try {
    const userRated = db.collection('movies').findOne({
      _id: new ObjectID(movieId),
      'ratings.user.$id': new ObjectID(session.getUser(cookie).id),
    })
    if (userRated) {
      ctx.status = 409
      ctx.body = { error: 'Already rated' }
    } else {
      await db
        .collection('movies')
        .updateOne(
          { _id: new ObjectID(movieId) },
          { $push: { ratings: movieRating } }
        )
      ctx.status = 202
      ctx.body = undefined
    }
  } catch (err) {
    ctx.status = 404
    ctx.body = err
    console.log('error', err)
  }
})

routes.get('/me', async (ctx: Context) => {
  const cookie = ctx.cookies.get('sessionId')
  console.log('cookie: ',cookie)
    if (cookie && session.getUser(cookie)) {
      ctx.response.body = session.getUser(cookie)
    } else {
    ctx.response.body = 'No user identified'
    ctx.status = 404
  }
})

module.exports = routes
