import { UserOutput } from './user.model'

interface sessionMap {
 [key: string]: UserOutput
}

export { sessionMap };
