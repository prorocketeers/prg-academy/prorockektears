import { UserOutput } from './user.model'
import { getUniqueId } from './routes'
import { sessionMap } from './sessionMap.model';

const fs = require('fs')
const path = './sessionMapSerialized.json'

export default class SessionMap {
  constructor() {
    fs.access(path, fs.F_OK, (err: any) => {
      if (err) {
        if (err.code === 'ENOENT') {
          console.log(`File ${path} does not exist, creating a new one.`)
          const fd = fs.openSync(path, 'w');
          fs.writeFileSync(fd, '{}');
        } else {
          console.error(err)
          return
        }
      }
      // file exists
      let rawdata = fs.readFileSync(path)
      this.sessionMap = JSON.parse(rawdata)
    })
  }

  sessionMap: sessionMap = {}

  getUser(sessionId: string) {
    const user: UserOutput = this.sessionMap[sessionId]
    return user
  }

  setUser(user: UserOutput) {
    const sessionId: string = getUniqueId()
    this.sessionMap[sessionId] = user

    const sessionMapString = JSON.stringify(this.sessionMap)
    fs.writeFile(path, sessionMapString, function(err: any) {
      if (err) {
        return console.error(err)
      }
      console.log('The file was saved!')
    })
    return sessionId
  }
}
