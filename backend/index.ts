import { Context } from 'koa'
require('dotenv').config()

// port the app is running on
const port = process.env.PORT

// imports of needed modules
const Koa = require('koa')
const tearsRoutes = require('./routes')
const bodyParser = require('koa-bodyparser')

const app = new Koa()

app.use(async (ctx: Context, next: Function) => {
  ctx.set('Access-Control-Allow-Origin', 'http://localhost:3000')
  ctx.set(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
  ctx.set('Access-Control-Allow-Credentials', 'true')
  await next()
})

app.use(bodyParser())

// router middleware
app.use(tearsRoutes.routes()).use(tearsRoutes.allowedMethods())

// listen on port
app.listen(port, () => {
  console.log(`app is running on port: ${port}`)
})
