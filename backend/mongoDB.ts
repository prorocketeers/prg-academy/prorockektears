import { MongoClient, Db } from 'mongodb'
require('dotenv').config()

const DB_NAME = process.env.DB_NAME

process.env

const DB_URL =
  process.env.DB_URL && process.env.DB_NAME
    ? process.env.DB_URL + process.env.DB_NAME
    : ''

let db: Db

export const getConnection = async () => {
  if (!db) {
    const client = new MongoClient(DB_URL)
    try {
      await client.connect()
      db = client.db(DB_NAME)
    } catch (e) {
      console.error(e)
    }
  }
  return db
}
