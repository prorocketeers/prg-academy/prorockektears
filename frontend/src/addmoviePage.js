import React, { Component } from "react"
import request from "./network"

export class AddMovie extends Component {
  request
  state = {
    title: "",
    imdbId: ""
  }

  handleChangetitle = event => {
    this.setState({ title: event.target.value })
  }
  handleChangeId = event => {
    this.setState({ imdbId: event.target.value })
  }

  render() {
    return (
      <div className="App">
        <form
          onSubmit={ event => {
            event.preventDefault()
            request.post('movies', {
              title: this.state.title,
              imdbId: this.state.imdbId
            })
          }
          }
        >
          <label>
            Title:
            <input
              id="Title"
              type="Text"
              placeholder="Title"
              value={this.state.title}
              onChange={this.handleChangetitle}
              required
            />
          </label>
          <label>
            ImdbId:
            <input
              id="ImdbId"
              type="Text"
              placeholder="ImdbId"
              value={this.state.imdbId}
              onChange={this.handleChangeId}
              required
            />
          </label>
          <input type="submit" value="Add movie" />
        </form>
      </div>
    )
  }
}
