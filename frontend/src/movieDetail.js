import React, { Component } from "react"
import request from "./network"

export class MovieDetail extends Component {
  state = {
    movie: {}
  }
  componentDidMount() {
    request.get(`movies${this.props.match.params.id}`)
      .then(response1 => {
        request.get(
            `http://www.omdbapi.com/?apikey=50ca5fb1&i=${response1.data.imdbId}`, { withCredentials: false }
          )
          .then(response2 => {
            const filmData = {
              title: response1.data.title,
              imdbId: response1.data.imdbId,
              rating:
                response1.data.ratings.reduce((acc, curr) => {
                  return (acc += curr.rating)
                }, 0) / response1.data.ratings.length,
              poster: response2.data.Poster,
              imdbRating: response2.data.imdbRating,
              movieDescription: response2.data.Plot
            }
            this.setState({ movie: filmData })
          })
      })
      .catch(error => console.log(error))
  }

  render() {
    const { movie } = this.state
    return (
      <div>
        <div key={movie.title}>
          <div>
            <img src={movie.poster} alt={this.state.movie.imdbId} />
          </div>
          <div>{movie.title}</div>
          <div>{movie.imdbRating}</div>
          <div>{movie.rating}</div>
          <div>{movie.movieDescription}</div>
        </div>
      </div>
    )
  }
}
