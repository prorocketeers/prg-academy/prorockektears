import React, { Component } from "react"
import request from './network'

export class RenderMovies extends Component {
  state = {
    movies: []
  }
  componentDidMount() {
    request.get('movies')
      .then(response => {
        const films = response.data
        films.forEach(film => {
          if (!film.imdbId) {
            return
          }
          request.get(`http://www.omdbapi.com/?apikey=50ca5fb1&i=${film.imdbId}`, { withCredentials: false })
            .then(response => {
              const filmData = {
                title: film.title,
                imdbId: film.imdbId,
                year: response.data.Year,
                poster: response.data.Poster
              }
              this.setState({
                movies: [...this.state.movies, filmData]
              })
            })
        })
      })
      .catch(error => console.log(error))
  }

  render() {
    return (
      <div>
        {this.state.movies.map(movie => {
          return (
            <div key={movie.imdbId}>
              <div>
                <img src={movie.poster} alt={movie.imdbId} />
              </div>
              <div>{movie.title}</div>
              <div>{movie.year}</div>
            </div>
          )
        })}
      </div>
    )
  }
}

//component will mount, constructor, componentwillupdate
