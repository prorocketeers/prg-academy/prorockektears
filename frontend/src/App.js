import React, { Component } from "react"
import "./App.css"
import LoginForm from "./LoginForm"
import { AddMovie } from "./addmoviePage"
import RegisterForm from "./registerForm"
import { BrowserRouter as Router, Route } from "react-router-dom"
import Header from "./Header"
import AppContext from "./AppContext"
import { RenderMovies } from "./renderMovies"
import { MovieDetail } from "./movieDetail"
import { routeConfig } from './config'
import request from './network'

class App extends Component {
  state = {
    user: {}
  }

  componentDidMount() {
    request.get('me')
      .then(response => {
        if (response.data) {
          this.setUser(response.data)
        }
      })
      .catch(console.error)
  }

  setUser = user => {
    console.log(user)
    this.setState({ user })
  }

  render() {
    return (
      <Router>
        <AppContext.Provider
          value={{ state: this.state, setUser: this.setUser }}
        >
          <Route component={Header} />
          <div className="App" id='background'>
            <Route path={routeConfig.login.path} exact component={LoginForm} />
            <Route path={routeConfig.register.path} exact component={RegisterForm} />
            <Route path={routeConfig.movieDetail.path} exact component={MovieDetail} />
            <Route path={routeConfig.addMovie.path} exact component={AddMovie} />
            <Route path={routeConfig.renderMovies.path} exact component={RenderMovies} />
          </div>
        </AppContext.Provider>
      </Router>
    )
  }
}

export default App
