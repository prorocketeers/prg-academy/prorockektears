import React, { Component } from "react"
import AppContext from "./AppContext"
import { Link } from "react-router-dom"
import PropTypes from "prop-types"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import CssBaseline from "@material-ui/core/CssBaseline"
import FormControl from "@material-ui/core/FormControl"
import Input from "@material-ui/core/Input"
import InputLabel from "@material-ui/core/InputLabel"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"
import withStyles from "@material-ui/core/styles/withStyles"
import "./backgroundstyle.css"
import request from "./network"

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 2
  }
})

class LoginForm extends Component {
  static contextType = AppContext

  state = { nickname: "", password: "" }

  handleChangeUserName = event => {
    this.setState({ nickname: event.target.value })
  }
  handleChangePassword = event => {
    this.setState({ password: event.target.value })
  }

  login = event => {
    event.preventDefault()
    request.post('users/login',
        {
          nickname: this.state.nickname,
          password: this.state.password
        },
    ).then(response => {
      this.context.setUser(response.data)
      this.props.history.push("/")
    })
  }

  render() {
    const { classes } = this.props
    return (
      <div id="background">
        <div id="backgroundOverlay">
          <main className={classes.main}>
            <CssBaseline />
            <Paper className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <form className={classes.form} onSubmit={this.login}>
                <FormControl margin="normal" required fullWidth>
                  <InputLabel htmlFor="userName">User Name</InputLabel>
                  <Input
                    id="UserName"
                    name="userName"
                    autoComplete="userName"
                    autoFocus
                    value={this.state.nickname}
                    onChange={this.handleChangeUserName}
                  />
                </FormControl>
                <FormControl margin="normal" required fullWidth>
                  <InputLabel htmlFor="password">Password</InputLabel>
                  <Input
                    name="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    value={this.state.password}
                    onChange={this.handleChangePassword}
                  />
                </FormControl>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Sign in
                </Button>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  component={Link}
                  to="/register"
                >
                  Register
                </Button>
              </form>
            </Paper>
          </main>
        </div>
      </div>
    )
  }
}

LoginForm.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(LoginForm)
