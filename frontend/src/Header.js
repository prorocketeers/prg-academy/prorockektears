import React, { Component } from "react"
import { Link } from "react-router-dom"
import AppContext from "./AppContext"
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { routeConfig } from './config'
import Cookies from 'js-cookie'


const styles = {
  root: {
    flexGrow: 1,
  },  

  menuItem: {
    marginRight: '3em',
  },

  padding: {
    paddingRight: 30,
    paddingBottom: 3,
    textDecoration: "none",
    marginRight: '3em',
  }
}

class Header extends Component {
  static contextType = AppContext

  logOut = e => {
    this.context.setUser({})
    Cookies.remove('sessionId')
    this.props.history.push('/')
  }

  render() {
    const { classes } = this.props
    let loggedIn = this.context.state.user.nickname
    // let loggedIn = 'Meeeee'
    if (loggedIn) {
      return (
        <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
              <Typography 
                variant="h6" 
                color="inherit" 
                className={classes.padding} 
                component={Link} 
                to="/"
              >
              ProMovies
              </Typography>
              <Button color="inherit" component={Link} to={routeConfig.renderMovies.path} className={classes.menuItem}>Movies</Button>
              <Button color="inherit" component={Link} to={routeConfig.addMovie.path} className={classes.menuItem}>Add a movie</Button>
              <span style={{ flexGrow: 1 }} />
              <Typography variant="h6" color="inherit" className={classes.menuItem}>
                {loggedIn}
              </Typography>
              <Button className={classes.menuItem} color="inherit" onClick={this.logOut}>Logout</Button>
          </Toolbar>
        </AppBar>
        </div>
      )
    } else {
      return (
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              <Typography 
                variant="h6" 
                color="inherit" 
                className={classes.padding} 
                component={Link} 
                to="/"
              >
              ProMovies
              </Typography>
              <span style={{ flexGrow: 1 }} />
              <Button className={classes.menuItem} color="inherit" component={Link} to={routeConfig.register.path}>Register</Button>
              <Button className={classes.menuItem} color="inherit" component={Link} to={routeConfig.login.path}>Login</Button>
            </Toolbar>
          </AppBar>
        </div>
      )
    }
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Header)
