import React, { Component } from "react"
import { genders } from './config'
import PropTypes from 'prop-types'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Slider from '@material-ui/lab/Slider';
import withStyles from '@material-ui/core/styles/withStyles'
import TextField from '@material-ui/core/TextField'
import './backgroundstyle.css'
import request from './network'

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  slider: {
    padding: '1px 0px',
    marginTop: "20px"
  },
  sliderText:{
    fontFamily:`-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif`,
  fontSize: "1rem",
  },
  emailInput:{
    marginBottom: "15px"
  }
})


class RegisterForm extends Component {
  state = {
    nickname: "",
    email: "",
    gender: 0,
    password: "",
    confirmPass: "",
    error: false,
    helperText: ""
  }

  handleChangeUserN = event => {
    this.setState({ nickname: event.target.value })
  }
  handleChangeEmail = event => {
    this.setState({ email: event.target.value })
  }
  handleChangeGender = (event,value) => {
    this.setState({ gender: value })
  }
  handleChangePassword = event => {
    this.setState({ password: event.target.value })
  }
  handleChangeConfPassword = event => {
    this.setState({ confirmPass: event.target.value })
  }

  validationPassword = () => {
    if(this.state.password !== this.state.confirmPass){
      this.setState({ error: true })
    }
    else{
      this.setState({error: false, helperText: ""})
    }
  }

  register = (event) => {
    event.preventDefault()
    if(this.state.password === this.state.confirmPass){
      request.post('users', {
        nickname: this.state.nickname,
        email: this.state.email,
        gender: this.state.gender,
        password: this.state.password
      })
      .then(() =>
      this.props.history.push("/login"))
    }
    else{
      this.setState({ helperText:"Passwords dont match!"})
    }
  }

  render() {
    const { classes } = this.props
    return (
      <div id="background">
      <div id="backgroundOverlay">
      <main className={classes.main}>
      <CssBaseline />
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.form} onSubmit={this.register}>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="userName">User name</InputLabel>
            <Input 
            id="userName" 
            name="userName" 
            autoFocus
            value={this.props.nickname} 
            onChange={this.handleChangeUserN}  
            required
            />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="email">Email address</InputLabel>
            <Input 
            className={classes.emailInput}
            name="email" 
            type="email" 
            id="email" 
            autoComplete="email"
            value={this.props.email}
            onChange={this.handleChangeEmail}
            required
            />
          </FormControl>
          <span className={classes.sliderText}>{genders[this.state.gender]}</span>
          <Slider
          classes={{ container: classes.slider }}
          min={0}
          max={31}
          step={1}
          value={this.state.gender}
          onChange={this.handleChangeGender}
          />
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input 
            name="password" 
            type="password"  
            autoComplete="current-password" 
            error= {this.state.error}
            value={this.props.password}
            onChange={this.handleChangePassword} 
            onBlur={this.validationPassword}
            required 
            />
          </FormControl>
          <TextField 
            margin="normal" 
            required 
            fullWidth
            name="password" 
            type="password"  
            autoComplete="current-password" 
            error= {this.state.error}
            value={this.props.confirmPass}
            onChange={this.handleChangeConfPassword} 
            onBlur={this.validationPassword}
            label="Confirm Password"
            helperText={this.state.helperText}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Register
          </Button>
        </form>
      </Paper>
    </main>
    </div>
    </div>
    )
  }
}

RegisterForm.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(RegisterForm)