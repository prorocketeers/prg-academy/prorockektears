# prorockektears

Match. Rate. Watch.

To run the app, create .env file in the backend folder with following content (values are just examples).
Notes:
- at the moment, frontend has port 3001 hardcoded
- db user and password should be blank if the DB is not password protected

DB_USERNAME=
DB_PASSWORD=
DB_NAME=moviesdb
DB_URL=mongodb://localhost:27017/
PORT=3001
